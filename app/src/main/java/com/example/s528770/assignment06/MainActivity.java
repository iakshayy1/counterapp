package com.example.s528770.assignment06;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s528770.assignment06.ColorDialog;
import com.example.s528770.assignment06.ConfirmationDialog;
import com.example.s528770.assignment06.R;

public class MainActivity extends AppCompatActivity implements ConfirmationDialog.ClearConfirmCallback, ColorDialog.ColorCallback{

    public int countref = 0;
    public int maxvalue = 0;
    public int clickscount = 0;

    TextView tv;
    TextView tv1;
    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        tv = (TextView) findViewById(R.id.counttv);
        tv1 = (TextView) findViewById(R.id.clickstv);
        maxvalue = getIntent().getIntExtra("maxval",0);

        ImageButton incbtn = (ImageButton) findViewById(R.id.incbtn);
        incbtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickscount++;
                if(maxvalue == 0){
                    countref++;
                    tv.setText("Count : " + countref);
                } else{
                    if(countref<maxvalue){
                        countref++;
                        tv.setText("Count : " + countref);
                    } else {
                        Toast t = Toast.makeText(getApplicationContext(), "Specified Maximum value reached", Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.RIGHT |Gravity.TOP, view.getLeft(), view.getTop()+(view.getBottom()-view.getTop())/2);
                        t.show();
                    }
                }
                tv1.setText("Clicks : " + clickscount);
            }

        });

        ImageButton decbtn = (ImageButton) findViewById(R.id.decbtn);
        decbtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickscount++;
                if (countref > 0) {
                    countref--;
                    tv.setText("Count : " + countref);
                } else {
                    Toast.makeText(getApplicationContext(), "Minimum Count Reached", Toast.LENGTH_SHORT).show();
                }
                tv1.setText("Clicks : " + clickscount);
            }
        });

        Button clrbtn = (Button) findViewById(R.id.clrbtn);
        clrbtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfirmationDialog d = new ConfirmationDialog();
                d.show(getSupportFragmentManager(), "Confirmation");
            }

        });

        Button cbgbtn = (Button) findViewById(R.id.cbgbtn);
        cbgbtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDialog d = new ColorDialog();
                d.show(getSupportFragmentManager(), "Color");
            }
        });

        int colorvalue = getIntent().getIntExtra("ref",10);

        if(colorvalue == 0){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color1));
        }if(colorvalue == 1){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color2));
        }if(colorvalue == 2){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color3));
        }if(colorvalue == 3){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color4));
        }if(colorvalue == 4){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color5));
        }
    }

    public void click(View v){
        clickscount++;
        if(maxvalue == 0){
            countref++;
            tv.setText("Count : " + countref);
        } else{
            if(countref<maxvalue){
                countref++;
                tv.setText("Count : " + countref);
            } else {
                Toast.makeText(getApplicationContext(), "Specified Maximum value reached", Toast.LENGTH_SHORT).show();
            }
        }
        tv1.setText("Clicks : " + clickscount);
    }

    @Override
    public void setcountref() {
        Toast.makeText(getApplicationContext(),"Cleared", Toast.LENGTH_SHORT).show();
        countref = 0;
        clickscount = 0;
        tv.setText("Count : " + countref);
        tv1.setText("Clicks : " + clickscount);
    }

    @Override
    public void colorcallback(int i) {
         if(i == 0){
             activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color1));
         }if(i == 1){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color2));
        }if(i == 2){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color3));
        }if(i == 3){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color4));
        }if(i == 4){
            activity.findViewById(android.R.id.content).setBackgroundColor(ContextCompat.getColor(this, R.color.color5));
        }
    }

    public void settings(View v){
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
    }


}
