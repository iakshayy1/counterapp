package com.example.s528770.assignment06;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Settings extends AppCompatActivity implements ColorDialog.ColorCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

    }

public int ref = 10;
    public int checkpoint = 0;

public void submit(View v){
    Intent main = new Intent(this, MainActivity.class);
    EditText et = (EditText) findViewById(R.id.editText);
//    if(!(et.getText().toString().equals(null))){
        int myNum;
        try {
            myNum = Integer.parseInt(et.getText().toString());
            if(myNum>0){
                checkpoint = 2;
                main.putExtra("maxval", myNum);
                startActivity(main);
            } else {
                Toast.makeText(getApplicationContext(),"Enter greater than zero", Toast.LENGTH_SHORT).show();
            }
        } catch(NumberFormatException nfe) {
            Toast.makeText(getApplicationContext(),"Enter a value", Toast.LENGTH_SHORT).show();
            checkpoint = 1;
            System.out.println("Could not parse " + nfe);
        }
        if(!(checkpoint == 1) && checkpoint == 2){
            if(!(ref == 10)){
                main.putExtra("ref", ref);
                startActivity(main);
            }
        }


}

public void changeBG(View v){
    ColorDialog d = new ColorDialog();
    d.show(getSupportFragmentManager(), "Color");
}


    @Override
    public void colorcallback(int i) {
        ref = i;

//        Intent z = new Intent(this, Settings.class);
//        ref = 1;
//        SharedPreferences  sp = getSharedPreferences("settings", Context.MODE_PRIVATE);
//        SharedPreferences.Editor edit = sp.edit();
//        edit.putInt("ref",ref);
//        edit.putInt("colorvalue", i);
    }
}
