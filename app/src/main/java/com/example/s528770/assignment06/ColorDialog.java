package com.example.s528770.assignment06;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by s528770 on 10/16/2017.
 */

public class ColorDialog extends android.support.v4.app.DialogFragment {
    public interface ColorCallback {
        void colorcallback(int i);
    }

    private ColorCallback myActivity;

    public void onAttach(Context context){
        super.onAttach(context);
        myActivity = (ColorCallback) context;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("pick a color");
        builder.setItems(R.array.color_menu_choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("color choice is", "selected" + i);
                myActivity.colorcallback(i);
            }
        });

        return builder.create();
    }
}
