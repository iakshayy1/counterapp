package com.example.s528770.assignment06;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by s528770 on 10/16/2017.
 */

public class ConfirmationDialog extends android.support.v4.app.DialogFragment {

    public interface ClearConfirmCallback {
        public void setcountref();
    }

    private ClearConfirmCallback myActivity1;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        myActivity1 = (ClearConfirmCallback) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("CLEAR");
        builder.setMessage("Do you want to clear count?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                myActivity1.setcountref();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder.create();
    }
}
