# CounterApp

#AndroidApp

Added more colors and change the items so that the color is display.

Added a settings activity.

Added a EditText that will allow the user specify a maximum value for the counter. Make sure to verify the maximum value should be an integer value greater than 0.

Moved the ColorChange Button to the Settings activity.

Added a second counter to count number of clicks made.

Changed the controls so that increasing the count can be done by clicking on the TextView.